FROM ubuntu:latest
ENV DEBIAN_FRONTEND=noninteractive
 
# Install dependencies
RUN apt update -qq \
    && apt upgrade -qq -y \
    && apt install -qq -y curl \
    && curl -sL https://deb.nodesource.com/setup_12.x | bash \
    && apt update -qq -y \
    && apt install -qq -y git jq xz-utils curl nodejs openjdk-8-jdk \
    && which ssh-agent || ( apt-get install -qq openssh-client )
 
ENV DEBIAN_FRONTEND=
 
# Install SFDX CLI and Salesforce CLI
ENV SALESFORCE_CLI_URL=https://developer.salesforce.com/media/salesforce-cli/sfdx-linux-amd64.tar.xz
ENV SFDX_AUTOUPDATE_DISABLE=false
ENV SFDX_USE_GENERIC_UNIX_KEYCHAIN=true
ENV SFDX_DOMAIN_RETRY=600
ENV SFDX_LOG_LEVEL=DEBUG
 
RUN mkdir sfdx \
    && curl -s $SALESFORCE_CLI_URL | tar xJ -C sfdx --strip-components 1 \
    && './sfdx/install' \
    && rm -r sfdx \
    && sfdx update \
    && sfdx --version \
    && npm install --global sfdx-cli \
    && echo y | sfdx plugins:install sfpowerkit @salesforce/sfdx-scanner