trigger totalContactTrigger on Contact (after insert, after update, after delete, after undelete) {
    if(Trigger.isAfter){
        if(Trigger.isInsert || Trigger.isUpdate){
            TotalContactTriggerHelper.afterInsertAndUpdate(Trigger.new, Trigger.oldMap);
        }
        
        if(Trigger.isDelete){
            TotalContactTriggerHelper.afterDelete(Trigger.old);
        }
    }

}