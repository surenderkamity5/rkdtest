/*When lead is created or updated then check if the email of lead is already there in existing contacts.
 If email already exist then throw error.
*/
trigger duplicateEmailInLeadTrigger on Lead (before insert, before update) {
    if(Trigger.isBefore){
        if(Trigger.isInsert || Trigger.isUpdate){
            DuplicateEmailInLeadTriggerHelper.checkDuplicateEmail(Trigger.new);
        }
    }

}