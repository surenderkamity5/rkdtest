trigger opportunityAmountTrigger on Opportunity (after insert, after update, after delete) {
    
    if(Trigger.isAfter){
        if(Trigger.isInsert || Trigger.isUpdate){
            OpportunityAmountTriggerHelper.afterInsertUpdate(Trigger.new, Trigger.oldMap);
        }
        else if(Trigger.isDelete){
            OpportunityAmountTriggerHelper.afterDelete(Trigger.old);
        }
    }

}