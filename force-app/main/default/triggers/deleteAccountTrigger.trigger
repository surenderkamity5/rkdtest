trigger deleteAccountTrigger on Account (before delete) {
    if(Trigger.isBefore && Trigger.isDelete){
        DeleteAccountTriggerHelper.beforeDelete(Trigger.old);
    }
}