trigger CountContactUsingAggregate on Contact (after insert, after update, after delete, after undelete) {
    //Storing old and new ids of account
    Set<Id> accIdSet = new Set<Id>();
    for(Contact con: Trigger.isDelete ? Trigger.old : Trigger.new){
        if(con.AccountId!=null){
            accIdSet.add(con.AccountId); 
        }
    }
    
    List<Account> newAccList = new List<Account>();
    for(AggregateResult ar :[SELECT AccountId AcctId, Count(id) TotalCount
                             FROM Contact
                             WHERE AccountId in: accIdSet
                             GROUP BY AccountId]){
                                 Account a = new Account();
                                 a.Id = (Id)ar.get('AcctId');
                                 a.Total_Count__c = (Integer)ar.get('TotalCount');
                                 newAccList.add(a);
                             }
    if(!newAccList.isEmpty()){
        update newAccList;
    }
    
    
    
}