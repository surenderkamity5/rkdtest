trigger DuplicateAccount on Account (before insert, before Update) {
     for (Account a : Trigger.new) {
        List<Account> existingAccounts = [SELECT Id FROM Account WHERE Name = :a.Name];
        if (!existingAccounts.isEmpty()) {
            a.Name.addError('You cannot create a duplicate Account.');
        }
    }
}