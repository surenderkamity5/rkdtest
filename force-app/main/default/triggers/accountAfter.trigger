/*Create related contact when Account is created 
 * Object- Account
 * Event - after
 * DML- insert
*/
trigger accountAfter on Account (after insert) {
    if(Trigger.isAfter && Trigger.isInsert)
    AccountAfterHelper.afterInsert(Trigger.new);

}