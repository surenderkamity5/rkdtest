trigger DeleteAccount on Account (before delete) {
    Set<Id> accIds = new Set<Id>();
    for(Account acc: trigger.old){
        accIds.add(acc.Id);
        
        
    }
    
    Map<Id,Account> newMap = new Map<Id,Account>([SELECT Id,(SELECT Id FROM Contacts)FROM Account WHERE Id IN:accIds]);
    for(Account a: trigger.old){
        if(newMap.get(a.id).contacts.size()>0){
            a.addError('Account can not be deleted');
        }
    }
        
    }