trigger updateAccountTrigger on Opportunity (after insert, after update) {
    if(Trigger.isAfter){
        if(Trigger.isInsert || Trigger.isUpdate){
            UpdateAccountTriggerHelper.updateRating(Trigger.new);
        }
    }

}