public class SkillBasedRouting {
    
    @InvocableMethod
    public static void routingCaseToAgent(List<String> caseIds){
        //create PSR(Pending service request)
        //Add skills to the request for the case
        //push it to Queue
        
        List<Case> caseInserted = [SELECT Id,Subject FROM Case WHERE Id =:caseIds];
        List<Skill> allSkills = [SELECT Id,MasterLabel FROM Skill];
        for(Case caseR: caseInserted){
            //PST represents the routing details of a work item that’s waiting to be routed or assigned.
            PendingServiceRouting psr = new PendingServiceRouting();
            psr.WorkItemId = caseR.id;
            psr.RoutingType = 'SkillsBased';
            psr.RoutingPriority = 1;
            psr.CapacityWeight =1;
            psr.ServiceChannelId = '0N95g0000009BGWCA2';
            psr.RoutingModel = 'MostAvailable';
            psr.IsReadyForRouting = FALSE;
            insert psr;
            
            //Find out the required skills 
            List<String> matchingSkillsIds = new List<String>();
            for(Skill mSkills: allSkills){
                if(caseR.Subject.contains(mSkills.MasterLabel)){
                    matchingSkillsIds.add(mSkills.Id);                   
                }      
            }
            
            List<SkillRequirement> skillsToInsert = new List<SkillRequirement>();
            //Associate matching skills to PSR
            for(String matchingSkillId:matchingSkillsIds){
                SkillRequirement skillReq = new SkillRequirement();
                skillReq.SkillId = matchingSkillId;
                skillReq.RelatedRecordId = psr.Id;
                skillReq.SkillLevel= 5;
                skillsToInsert.add(skillReq);            
            }
            insert skillsToInsert;
            //push our request in to queue
            psr.IsReadyForRouting = TRUE;
            update psr;     
        }            
    }
}