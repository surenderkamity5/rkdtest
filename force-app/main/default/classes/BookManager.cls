@RestResource(urlMapping='/v53/BookManagement/')
global with sharing class BookManager {
    
    @HttpGet
    global static Book__c doGetBook(){
        Book__c book = new Book__c();
        Map<String,String> paramsMap = RestContext.request.params;
        String bookId = paramsMap.get('Id');
        book = [SELECT Id,Name,Price__c FROM Book__c WHERE ID =: bookId];
        
        return book;
        
    }
    
    @HttpDelete
    global static String doDelete(){
         Book__c book = new Book__c();
        Map<String,String> paramsMap = RestContext.request.params;
        String bookId = paramsMap.get('Id');
        book = [SELECT Id,Name,Price__c FROM Book__c WHERE ID =: bookId];
        delete book;
        return 'Record is deleted';
        
    }
    
    @HttpPost
    global static Book__c doPost(String name){
        Book__c book = new Book__c(Name = name);
        insert book;  
        return book;     
    }
    
    @HttpPut
    global static Book__c doPut(String name){
        Map<String,String> paramsMap = RestContext.request.params;
        String bookId = paramsMap.get('Id');
        Book__c book = new Book__c(Name = name, Id=bookId);
        update book;
        return book;
    }

}