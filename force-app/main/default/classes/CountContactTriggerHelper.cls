public class CountContactTriggerHelper {
    
    public static void countContact(List<Contact> newList, List<Contact> oldList){
        Set<Id> accIds = new Set<Id>();
        if(newList!= null){
            for(contact con: newList){
                if(con.AccountId!= null){
                    accIds.add(con.AccountId);
                }
            }
        }
        
        if(oldList!= null){
            for(contact con: oldList){
                if(con.AccountId!= null){
                    accIds.add(con.AccountId);
                }
            }
        }
        
        List<Account> accList = [SELECT Id, Total_Count__c,(SELECT Id from contacts)from Account where Id IN:accIds];
        if(accList!=null){
            for(Account acc: accList){
                acc.Total_Count__c = acc.contacts.size();
            }
        }
        
        if(!accList.isEmpty()){
           update accList; 
        }      
    }
}