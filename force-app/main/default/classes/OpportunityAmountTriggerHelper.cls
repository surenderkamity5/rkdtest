public class OpportunityAmountTriggerHelper {
    public static List<Account> accListUpdated = new List<Account>();
    public static Set<Id> accIdSet = new Set<Id>();
    
    public static void afterInsertUpdate(List<Opportunity> newList, Map<Id,Opportunity> oldMap){    
        for(Opportunity opp: newList){
            if(opp.AccountId!=null){  
                accIdSet.add(opp.AccountId); //Storing Accountid after insert             
            }
            if(trigger.isUpdate){   
                //if(opp.AccountId!=null && opp.AccountId != oldMap.get(opp.id).AccountId)
                if(opp.AccountId != NULL && opp.AccountId != oldMap.get(opp.Id).AccountId){
                     accIdSet.add(oldMap.get(opp.Id).AccountId);
                }
            }
            
        }
        calculateSumOfAmount(accIdSet);    
    }
    
    public static void afterDelete(List<Opportunity> oldList){
        for(Opportunity opp: oldList){
            if(opp.AccountId!=null){
                accIdSet.add(opp.AccountId); //Storing Accountid after delete
            }
        }
        calculateSumOfAmount(accIdSet);    
    }
    
    //Calculating sum of amount.
    public static void calculateSumOfAmount(Set<Id> accIdSet){
        Map<Id,Decimal> accIdToSumOfAmount = new Map<Id,Decimal>();
        List<Account> newAccList = [Select Id,Sum_of_amount__c,(SELECT Id,AccountId, Amount FROM Opportunities)FROM Account Where Id IN:accIdSet];
        if(!newAccList.isEmpty()){
            for(Account acc: newAccList){
                for(Opportunity opp: acc.opportunities){
                    Decimal sum=0;
                    if(accIdToSumOfAmount.containsKey(opp.AccountId)){
                        sum = accIdToSumOfAmount.get(opp.AccountId); //sum=0 for first opportunity.
                    }
                    if(opp.Amount !=null){
                        sum= sum+ opp.Amount;
                    }
                    accIdToSumOfAmount.put(opp.AccountId, sum);
                    acc.Sum_of_amount__c = accIdToSumOfAmount.get(opp.AccountId);
                    if(!accListUpdated.contains(acc)){
                        accListUpdated.add(acc);
                    }              
                }            
            }
        }     
        if(!accListUpdated.isEmpty()){
            update accListUpdated;
        }     
    }   
}