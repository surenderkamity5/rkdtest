public class PopulateCityHelper {
    public static void updateCityOnAccount(List<Account> newAccList){
        //Map<Id,Account> mapIdToAcc = new Map<Id,Account>();
        Set<Decimal> pincodeSet = new Set<Decimal>(); 
        System.debug('Hi');
        for(Account acc:newAccList){
            System.debug('Hi');
            if(acc.Pin_Code__c!=null){
                System.debug('Hi');
                pincodeSet.add(acc.Pin_Code__c); 
            }
            
            
        }
        List<String> cityList = new List<String>();
        List<MsPincode__c> newMsList = [SELECT Id,City__c,Pin_Code__c from MsPincode__c WHERE Pin_Code__c IN:pincodeSet];
        for(MsPincode__c mp:newMsList){
            /*if(mp.City__c!=null){
if(mp.Pin_Code__c == mapIdToAcc.get(acc.id).Pin_Code__c){
mapIdToAcc.get(acc.id).City__c = mp.City__c;
mapIdToAcc.put(acc.id, acc);                    
}
*/
            if(mp.City__c!=null){
                for(Account acc: newAccList){
                    if(acc.Pin_Code__c == mp.Pin_Code__c){
                        acc.City__c = mp.City__c;
                        
                    }
                }
                
            }  
        }
        
    }
    
    
}