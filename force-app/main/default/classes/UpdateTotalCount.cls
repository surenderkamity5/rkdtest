public class UpdateTotalCount {
    // after insert
    public void afterInsert(Map<Id,Contact> newMap)
    {
        Set<Id> accIds = new Set<Id>();
        for(Contact con: newMap.values())
        {
            // check contacts have acoount
            if(con.AccountId !=null){
               accIds.add(con.AccountId);
            }
            
        }
        //. call update account actions
        if(accIds.size() > 0){
            THIS.updateAccounts(accIds);
        }
        
    }
   //After delete
    public void afterDelete(Map<Id,Contact> oldMap)
    {
        Set<Id> accIds = new Set<Id>();
        for(Contact con: oldMap.values())
        {
            // check contacts have acoount
            if(con.AccountId !=null){
               accIds.add(con.AccountId);
            }
            
        }
        //. call update account actions
        if(accIds.size() > 0){
            THIS.updateAccounts(accIds);
        }
        
    }
     //After Undelete
    public void afterUndelete(Map<Id,Contact> newMap)
    {
        Set<Id> accIds = new Set<Id>();
        for(Contact con: newMap.values())
        {
            // check contacts have acoount
            if(con.AccountId !=null){
               accIds.add(con.AccountId);
            }
            
        }
        //. call update account actions
        if(accIds.size() > 0){
            THIS.updateAccounts(accIds);
        }
        
    }
    
// Account to be updated
private void updateAccounts(Set<Id> accountIds){
    List<Account> accList= [SELECT id, Total_Count__c,(SELECT id FROM Contacts) FROM Account WHERE ID IN: accountIds];
    
    for(Account acc: accList){
        acc.Total_Count__c = acc.contacts.size();
    }
    if(!accList.isEmpty()){
        update accList;
    }
}
}