public class UpdateAccountTriggerHelper {
    
    public static void updateRating(List<Opportunity> newList){ 
        Set<Id> accIdSet = new Set<Id>();
        for(Opportunity opp: newList){  
            if(opp.StageName == 'Closed Won'){
                accIdSet.add(opp.AccountId);      
            }
        }
        
        List<Account> accList;
            for(Account acc:[Select Id, Rating FROM Account Where Id IN:accIdSet]){
                acc.Rating = 'Hot';
                accList.add(acc);
            }   
        if(accList !=null){
            update accList;
        }
              
    }   
}