public class ApexActionFromFlow {
    
     @InvocableMethod(label='Send Email to Contact'
                    description='Send Email to contact'
                    category='Case')
    
    public static void sendEmail(List<Requests> requests){
        String contactIds = requests.get(0).contactIds;
        String caseNumber = requests.get(0).caseNumber;
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toaddresses = new String[]{contactIds};
            message.subject = 'Your case has been received'+caseNumber;
        message.plaintextbody = 'Your case has been received';                                  
        message.setSaveAsActivity(true);
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>{message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        if(results[0].success){
            System.debug('The mail was send successfully');
            
        }
        else{
            System.debug('Email failed to send' +results[0].errors[0].message);
        }
    }
    
    public class Requests{
        @InvocableVariable(label='Case record id' Description = 'Record id of case' required =true)
        public String caseIds;
        @InvocableVariable(label='Contact record id' Description = 'Record id of contact' required =true)
            public String contactIds;
        @InvocableVariable(label='Case Number' Description = 'The case number' required =true)
        public String caseNumber;
    }

}