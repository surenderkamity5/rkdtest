public class AccountAfterHelper {
    public static void afterInsert(List<Account> newList){
        List<Contact> con = new List<Contact>();
        for(Account acc:newList){
            Contact c= new Contact();
            c.AccountId = acc.id;
            c.LastName = acc.Name;
            con.add(c);
        }
        if(con!=null){
            insert con;
        }
    }

}