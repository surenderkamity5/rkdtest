public class ConUp {
    public void afterInsert(Map<Id,Contact> newMap){
        Set<Id> accIds = new Set<Id>();
        for(Contact con: newMap.values()){
            if(con.AccountId !=null){
                accIds.add(con.AccountId);
                
                
            }
        }
        
        if(!accIds.isEmpty()){
            THIS.updateAccount(accIds);
        }
    }
    
    //after delete
     public void afterDelete(Map<Id,Contact> oldMap){
        Set<Id> accIds = new Set<Id>();
        for(Contact con: oldMap.values()){
            if(con.AccountId !=null){
                accIds.add(con.AccountId);
                
                
            }
        }
        
        if(!accIds.isEmpty()){
            THIS.updateAccount(accIds);
        }
    }
    
     //after undelete
     public void afterUndelete(Map<Id,Contact> newMap){
        Set<Id> accIds = new Set<Id>();
        for(Contact con: newMap.values()){
            if(con.AccountId !=null){
                accIds.add(con.AccountId);
                
                
            }
        }
        
        if(!accIds.isEmpty()){
            THIS.updateAccount(accIds);
        }
    }
    
    
   
    private void updateAccount(Set<Id> accountId){
        List<Account> newList = [SELECT Id,name,(SELECT Id,name FROM Contacts)FROM Account WHERE ID=:accountId];
        for(Account acc:newList){
            acc.Total_Count__c = acc.contacts.size();
        }
        if(!newList.isEmpty()){
            update newList;
        }
    }

}