public class ScheduleBatchClass implements Schedulable {
    public void execute(SchedulableContext sc){
        //Database.executeBatch(new DeleteAccountRecord());
        DeleteAccountRecord r = new DeleteAccountRecord();
        Database.executeBatch(r);
    }

}