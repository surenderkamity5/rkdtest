public class FlowSeries {
    
    @InvocableMethod(label='Deleted related opportunities'
                    description='Deleted related opportunities for the given ids'
                    category='Account')
    public static void deleteOpportunities(List<String> ids){
        List<Opportunity> oppList = [SELECT Id FROM Opportunity WHERE AccountId =:ids AND StageName ='Closed Won'];
        delete oppList;
    }

}