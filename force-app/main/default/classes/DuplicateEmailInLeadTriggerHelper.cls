public class DuplicateEmailInLeadTriggerHelper {
    public static void checkDuplicateEmail(List<Lead> newList){
        Map<Id,String> mapIdToString = new Map<Id,String>();
        for(Lead l :newList){
            if(l.Email != null){
                mapIdToString.put(l.Id, l.Email);
                
            }
        }
        
        List<Contact> conList = [SELECT Id, Email FROM Contact WHERE Id IN: mapIdToString.keySet()];
    }

}