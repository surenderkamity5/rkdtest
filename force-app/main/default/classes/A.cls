//Added discription

public class A {
    public void afterInsert(Map<Id,Contact> newMap){
        Set<Id> accIds = new Set<Id>();
        for(Contact c: newMap.values()){
            if(c.AccountId!=null){
                accIds.add(c.AccountId);
            }
        }
        if(accIds!=null){
            THIS.updateAcc(accIds);
        }
    }
    
    public void afterUpdate(Map<Id,Contact> newMap){
        Set<Id> accIds = new Set<Id>();
        for(Contact c: newMap.values()){
            if(c.AccountId!=null){
                accIds.add(c.AccountId);
            }
        }
        if(accIds!=null){
            THIS.updateAcc(accIds);
        }
    }
    
    public void afterDelete(Map<Id,Contact> oldMap){
        Set<Id> accIds = new Set<Id>();
        for(Contact c: oldMap.values()){
            if(c.AccountId!=null){
                accIds.add(c.AccountId);
            }
        }
        if(accIds!=null){
            THIS.updateAcc(accIds);
        }
    }
    
    private void updateAcc(Set<Id> accountids){
        List<Account> accList = [SELECT Id, Total_Count__c,(SELECT Id FROM Contacts)FROM Account WHERE Id IN:accountids];
        if(accList.size()>0){
            for(Account acc: accList){
                acc.Total_Count__c = acc.contacts.size();
                
                
            }
    }
        if(accList.size()>0){
            update accList;
        }
    }

}