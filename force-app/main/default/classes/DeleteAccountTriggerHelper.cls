public class DeleteAccountTriggerHelper {
    public static void beforeDelete(List<Account> oldList){
        Set<Id> accIdSet = new Set<Id>();
        for(Account acc: oldList){
            accIdSet.add(acc.id);
        }
        List<Account> accList = [SELECT Id,(SELECT Id FROM contacts)FROM Account WHERE Id IN:accIdSet];
        Map<Id,Account> mapIdToAcc = new Map<Id,Account>();
        for(Account acc:accList){
            mapIdToAcc.put(acc.Id,acc); //Here, using map to avoid nested for loop
        }
        for(Account acc:oldList){
            if(mapIdToAcc.get(acc.Id).contacts.size()>1){
                acc.addError('You can not delete account');
            }
        }
    }    
}