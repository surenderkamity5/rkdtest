@RestResource(urlMapping='/Opportunities/')
global with sharing class OpportunityManager {
    @HttpGet
    global static Opportunity getOpportunityById(){
        RestRequest request = RestContext.request;
        //grab the oppId from the end of url
        String oppId = request.requestURI.substring(request.requestURI.lastIndexOf('/')+1);
        Opportunity result = [SELECT Name,LeadSource, CloseDate, Probability FROM Opportunity WHERE ID=: oppId];
        return result;
        
    }
    
    @HttpPost
    global static ID createOpportunity(String name, String stage, String closeDate, String source){
        Opportunity newOpportunity = new Opportunity(name=name,stageName=stage, closedate=Date.valueOf(closedate),leadsource=source
                                                   );
        insert newOpportunity;
        return newOpportunity.Id;
    }

}