global class DeleteAccountRecord implements Database.Batchable<sObject>{
    
    //global final String query;    
    //query = 'SELECT Id, name FROM Account LIMIT 10';
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id, name FROM Account LIMIT 10');
    }
    
    global void execute(Database.BatchableContext bc, List<Account> scope){
        delete scope;   
    }
    
    global void finish(Database.BatchableContext bc){
        // get the ID of the Asyncapexjob representing this batch job
        // Query the AsyncApexJob object to retrieve the current job's information
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id =:bc.getJobId()];
        
       //Send an email 
       Messaging.SingleEmailMessage mail = new  Messaging.SingleEmailMessage();
       String[] toAddress = new String[]
       {
          a.CreatedBy.Email
       };
           mail.setToAddresses(toAddress);
         mail.setSubject('Deleted record notification');
        mail.setPlainTextBody('Record Processed' + a.TotalJobItems + 'with' +a.NumberOfErrors+ 'Failures.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        
    }

}